package Employee;

import java.util.Date;

public class Employee {
    private int id;
    private String name;
    private boolean managerFlag;
    private boolean part_timeFlag;
    private Date dateHired;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateHired() {
		return dateHired;
	}
	public void setDateHired(Date dateHired) {
		this.dateHired = dateHired;
	}
	public boolean isManager() {
		return managerFlag;
	}
	public void setManager(boolean managerFlag) {
		this.managerFlag = managerFlag;
	}
    //this will return true if the employee is part-time
	public boolean isPart_time() {
		return part_timeFlag;
	}
    //this will set the part-time-flag to true or false
	public void setPart_time(boolean part_timeFlag) {
		this.part_timeFlag = part_timeFlag;
	}
    
}